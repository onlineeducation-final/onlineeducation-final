<?php  
 if(isset($_POST["employee_id"]))  
 {  
      $output = '';  
      require("conn.php");
      mysqli_query($conn,"SET CHARACTER SET UTF8");  
      $query = "SELECT subject.subject_engname,exampapers.exampapers_id,exampapers.exampapers_name,
      exampapers.exampapers_category,exampapers.exampapers_status,exampapers_type,exampapers_date,exampapers_enddate
      FROM exampapers 
      INNER JOIN coursesopen ON exampapers.exampapers_coursesopen_id=coursesopen.coursesopen_id
      INNER JOIN subject ON coursesopen.coursesopen_subject_id=subject.subject_id
      WHERE exampapers_id= '".$_POST["employee_id"]."'";
      $result = mysqli_query($conn, $query);  
      $output .= '  
      <div class="table-responsive">   
           <table class="table table-bordered">';  
      while($row = mysqli_fetch_array($result))  
      {   
          if ($row["exampapers_category"]==1) {
               $ec="สอบย่อย";
           }
           else if ($row["exampapers_category"]==2) {
               $ec="สอบกลางภาค";
           }
           else if ($row["exampapers_category"]==3) {
               $ec="สอบปลายภาค";
           }

          if ($row["exampapers_type"]==1) {
               $et="ข้อสอบอัตนัย";
           }
           else if ($row["exampapers_type"]==2) {
               $et="ข้อสอบสอบปรนัย";
           }
           
           $output .= '  
                <tr>  
                     <td width="30%"><label>รายวิชา</label></td>  
                     <td width="70%">'.$row["subject_engname"].'</td>  
                </tr>  
                <tr>  
                     <td width="30%"><label>ชื่อเอกสารสอบ</label></td>  
                     <td width="70%">'.$row["exampapers_name"].'</td>  
                </tr>  
                <tr>  
                     <td width="30%"><label>ประเภทการสอบ</label></td>  
                     <td width="70%">'.$ec.'</td>  
                </tr>  
                <tr>  
                     <td width="30%"><label>ประเภทข้อสอบ</label></td>  
                     <td width="70%">'.$et.'</td>  
                </tr>   
                <tr>  
                     <td width="30%"><label>วันที่เริ่มสอบ</label></td>  
                     <td width="70%">'.$row["exampapers_date"].'</td>  
                </tr>  
                <tr>  
                     <td width="30%"><label>วันที่ส่งข้อสอบ</label></td>  
                     <td width="70%">'.$row["exampapers_enddate"].'</td>  
                </tr>    
           ';  
      }  
      $output .= '  
           </table>  
      </div>  
      ';  
      echo $output;  
 }  
//  if ($row['preName_status'] == "1") {
//      echo "<a style='color:#228B22;'>เปิดการใช้งาน</a>";
//   }
//  else{
//     echo "<a style='color:red;'>ปิดการใช้งาน</a>";
//  }
 ?>