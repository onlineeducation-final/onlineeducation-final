<?php
    class DbConnect {
		private $host = '20.198.218.66';
		private $dbName = 'onlineeducations';
		private $user = 'root';
		private $pass = 'mynewpassword';

		public function connect() {
			try {
				$conn = new PDO('mysql:host=' . $this->host . '; dbname=' . $this->dbName, $this->user, $this->pass);
				$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				return $conn;
			} catch( PDOException $e) {
				echo 'Database Error: ' . $e->getMessage();
			}
		}
	}

?>