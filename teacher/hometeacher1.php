<?php
  session_start();
  if (!isset($_SESSION['teacher_username'])) {
    header('location: ../login.php');
  }
  if (isset($_GET['logout'])) {
    session_destroy();
    unset($_SESSION['teacher_username']);
    header('location: ../index.php');
  }
  require("conn.php");
  $username=$_SESSION['teacher_username'];
  $sql="SELECT prename.preName_name,teacher.teacher_fname,teacher.teacher_lname,teacher.teacher_phone,
  teacher.teacher_email,univercity.univercity_thname,faculty.faculty_name,department.department_name,
  teacher.teacher_username,teacher.teacher_password,teacher.teacher_status
  FROM teacher 
  INNER JOIN prename ON teacher.teacher_prename_id =prename.preName_id INNER JOIN univercity ON teacher.teacher_univercity_id=univercity.univercity_id 
  INNER JOIN faculty ON teacher.teacher_faculty_id =faculty.faculty_id 
  INNER JOIN department ON teacher.teacher_department_id=department.department_id 
  WHERE teacher_username='$username'";
  $result=mysqli_query($conn,$sql);

    $std= mysqli_fetch_assoc(mysqli_query($conn,"SELECT COUNT(*) as totalstd FROM student"));

    $doc= mysqli_fetch_assoc(mysqli_query($conn,"SELECT COUNT(*) as totaldoc FROM document "));

    $op= mysqli_fetch_assoc(mysqli_query($conn,"SELECT COUNT(*) as totalop FROM coursesopen"));

    $sub= mysqli_fetch_assoc(mysqli_query($conn,"SELECT COUNT(*) as totalsub FROM subject"));

    $query=mysqli_query($conn,"SELECT COUNT(coursesopen_id) FROM `coursesopen` INNER JOIN subject ON coursesopen.coursesopen_subject_id=subject.subject_id 
    INNER JOIN teacher ON coursesopen.coursesopen_teacher_id=teacher.teacher_id
    WHERE teacher_username='$username' ");

$sql="SELECT coursesopen.coursesopen_id,subject.subject_engname,coursesopen.coursesopen_term,coursesopen.coursesopen_schoolyear,teacher.teacher_fname,teacher.teacher_lname,coursesopen.coursesopen_status 
FROM coursesopen 
INNER JOIN subject ON coursesopen.coursesopen_subject_id=subject.subject_id 
INNER JOIN teacher ON coursesopen.coursesopen_teacher_id=teacher.teacher_id
WHERE teacher_username='$username'";
$result73 = mysqli_query($conn,$sql);

$row = mysqli_fetch_row($query);

$rows = $row[0];

$page_rows = 2;  //จำนวนข้อมูลที่ต้องการให้แสดงใน 1 หน้า  ตย. 3 record / หน้า 

$last = ceil($rows/$page_rows);

if($last < 1){
  $last = 1;
}

$pagenum = 1;

if(isset($_GET['pn'])){
  $pagenum = preg_replace('#[^0-9]#', '', $_GET['pn']);
}

if ($pagenum < 1) {
  $pagenum = 1;
}
else if ($pagenum > $last) {
  $pagenum = $last;
}

$limit = 'LIMIT ' .($pagenum - 1) * $page_rows .',' .$page_rows;

$nquery=mysqli_query($conn,"SELECT coursesopen.coursesopen_id,subject.subject_engname,coursesopen.coursesopen_term,coursesopen.coursesopen_schoolyear,teacher.teacher_fname,teacher.teacher_lname,coursesopen.coursesopen_status 
FROM coursesopen 
INNER JOIN subject ON coursesopen.coursesopen_subject_id=subject.subject_id 
INNER JOIN teacher ON coursesopen.coursesopen_teacher_id=teacher.teacher_id
WHERE teacher_username='$username' $limit");

$paginationCtrls = '';

if($last != 1){

if ($pagenum > 1) {
      $previous = $pagenum - 1;
              $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$previous.'" class="btn btn-info" style="font-family: Kanit, sans-serif;"><-</a> &nbsp; &nbsp; ';
      
              for($i = $pagenum-4; $i < $pagenum; $i++){
                  if($i > 0){
              $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'" class="btn btn-primary" style="font-family: Kanit, sans-serif;">'.$i.'</a> &nbsp; ';
                  }
          }
      }
      
          $paginationCtrls .= ''.$pagenum.' &nbsp; ';
      
          for($i = $pagenum+1; $i <= $last; $i++){
              $paginationCtrls .= '<a href="'.$_SERVER['PHP_SELF'].'?pn='.$i.'" class="btn btn-primary" style="font-family: Kanit, sans-serif;">'.$i.'</a> &nbsp; ';
              if($i >= $pagenum+4){
                  break;
              }
          }
      
      if ($pagenum != $last) {
      $next = $pagenum + 1;
      $paginationCtrls .= ' &nbsp; &nbsp; <a href="'.$_SERVER['PHP_SELF'].'?pn='.$next.'" class="btn btn-info" style="font-family: Kanit, sans-serif;">-></a> ';
      }
          }

  mysqli_query($conn,"SET CHARACTER SET UTF8");
?>
<!DOCTYPE html>
<!-- Designined by CodingLab | www.youtube.com/codinglabyt -->
<html lang="en" dir="ltr">
  <head>
  <meta charset="UTF-8">
    <title> Online Education </title>
    <link rel="stylesheet" href="menu/menu.css">
    <link rel="shortcut icon" type="image/x-icon" href="../assets1/images/logo3.png">
    <!-- Boxiocns CDN Link -->
    <link href='https://unpkg.com/boxicons@2.0.7/css/boxicons.min.css' rel='stylesheet'>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Kanit&display=swap" rel="stylesheet">
     <meta name="viewport" content="width=device-width, initial-scale=1.0">
     <link href="Prename1.css" rel="stylesheet">
     <link href="../demo/style.css" rel="stylesheet">
     <script src="../demo/main.js"></script>
     <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta3/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.min.css" />
    <link href="https://cdn.jsdelivr.net/npm/remixicon@2.5.0/fonts/remixicon.css" rel="stylesheet">
     <style>
       :root {
  --main-bg-color: #009d63;
  --main-text-color: #1B2631;
  --second-text-color: #bbbec5;
  --second-bg-color: #AED6F1;
}

.primary-text {
  color: var(--main-text-color);
}

.second-text {
  color: var(--second-text-color);
}

.primary-bg {
  background-color: var(--main-bg-color);
}

.secondary-bg {
  background-color: var(--second-bg-color);
}

.rounded-full {
  border-radius: 100%;
}

#wrapper {
  overflow-x: hidden;
  background-image: linear-gradient(
    to right,
    #baf3d7,
    #c2f5de,
    #cbf7e4,
    #d4f8ea,
    #ddfaef
  );
}

#sidebar-wrapper {
  min-height: 100vh;
  margin-left: -15rem;
  -webkit-transition: margin 0.25s ease-out;
  -moz-transition: margin 0.25s ease-out;
  -o-transition: margin 0.25s ease-out;
  transition: margin 0.25s ease-out;
}

#sidebar-wrapper .sidebar-heading {
  padding: 0.875rem 1.25rem;
  font-size: 1.2rem;
}

#sidebar-wrapper .list-group {
  width: 15rem;
}

#page-content-wrapper {
  min-width: 100vw;
}

#wrapper.toggled #sidebar-wrapper {
  margin-left: 0;
}

#menu-toggle {
  cursor: pointer;
}

.list-group-item {
  border: none;
  padding: 20px 30px;
}

.list-group-item.active {
  background-color: transparent;
  color: var(--main-text-color);
  font-weight: bold;
  border: none;
}

@media (min-width: 768px) {
  #sidebar-wrapper {
    margin-left: 0;
  }

  #page-content-wrapper {
    min-width: 0;
    width: 100%;
  }

  #wrapper.toggled #sidebar-wrapper {
    margin-left: -15rem;
  }
}
.columns {
      border-color: black;
      border-radius: 10px;
      /* background-color: #fff; */
      /* padding: 10px 10px; */
      text-align: center;
      text-decoration: none;
      display: inline-block;
      font-size: 15px;
      /* margin: 4px 2px; */
      cursor: pointer;
      font-family: 'Kanit', sans-serif;
      width: 110px;
      height: 62px;
  }
/* Style the tab */
.tab {
  overflow: hidden;
  border: 1px solid #ccc;
  background-color: #f1f1f1;
}

/* Style the buttons inside the tab */
.tab button {
  background-color: inherit;
  float: left;
  border: none;
  outline: none;
  cursor: pointer;
  padding: 14px 16px;
  transition: 0.3s;
  font-size: 17px;
}

/* Change background color of buttons on hover */
.tab button:hover {
  background-color: #ddd;
}

/* Create an active/current tablink class */
.tab button.active {
  background-color: #ccc;
}

/* Style the tab content */
.tabcontent {
  display: none;
  padding: 6px 12px;
  border: 1px solid #ccc;
  border-top: none;
}
     </style>
   </head>
<body>
  <div class="sidebar close">
  <div class="logo-details">
      <i><img src="image/logo1.png" alt="profileImg" style="width: 40px;  height:40px;"></i>
      <!-- <img src="image/logo1.png" alt="profileImg" style="width: 50px;  height:12px;"> -->
      <span class="logo_name">MSU Education</span>
      <!-- <img src="image/logo.png" alt="profileImg" style="width: 150px;  height:212px; float:top;"> -->
    </div>
    <ul class="nav-links">
      <li>
        <a href="hometeacher1.php">
          <i class='bx bx-grid-alt' ></i>
          <span class="link_name" style="font-family: 'Kanit', sans-serif;">หน้าหลัก</span>
        </a>
        <ul class="sub-menu blank">
          <li><a class="link_name" href="hometeacher1.php" style="font-family: 'Kanit', sans-serif;">หน้าหลัก</a></li>
        </ul>
      </li>
      <li>
        <a href="std.php">
          <!-- <i class='bx bx-line-chart' ></i> -->
          <i class='bx bx-user' ></i>
          <span class="link_name" style="font-family: 'Kanit', sans-serif;">ข้อมูลนิสิต</span>
        </a>
        <ul class="sub-menu blank">
          <li><a class="link_name" href="../std.php" style="font-family: 'Kanit', sans-serif;">ข้อมูลนิสิต</a></li>
        </ul>
      </li>
      <li>
        <div class="iocn-link">
          <a href="#">
            <i class='bx bx-book-alt' ></i>
            <span class="link_name" style="font-family: 'Kanit', sans-serif;">การทำงานอาจารย์</span>
          </a>
          <i class='bx bxs-chevron-down arrow' ></i>
        </div>
        <ul class="sub-menu">
          <li><a class="link_name" href="#" style="font-family: 'Kanit', sans-serif;">การทำงานอาจารย์</a></li>
          <li><a href="opensubject.php" style="font-family: 'Kanit', sans-serif;">- รายวิชาที่เปิดสอน</a></li>
          <li><a href="addstudentinsubject.php" style="font-family: 'Kanit', sans-serif;">- นิสิตในรายวิชา</a></li>
          <li><a href="../adddocument.php" style="font-family: 'Kanit', sans-serif;">- เอกสารการสอน</a></li>
          <li><a href="../addvdo.php" style="font-family: 'Kanit', sans-serif;">- วีดิทัศน์</a></li>
          <li><a href="../addexam.php" style="font-family: 'Kanit', sans-serif;">- แบบฝึกหัด</a></li>
          <li><a href="addstream.php" style="font-family: 'Kanit', sans-serif;">- ไลฟ์</a></li>
          <li><a href="exampaper.php" style="font-family: 'Kanit', sans-serif;">- ข้อสอบ</a></li>
          <li><a href="checkexam.php" style="font-family: 'Kanit', sans-serif;">- ตรวจข้อสอบ</a></li>
          <li><a href="news.php" style="font-family: 'Kanit', sans-serif;">- ข่าวสาร</a></li>
        </ul>
      </li>
      <li>
        <div class="iocn-link">
          <a href="#">
          <i class='bx bx-data'></i>
            <span class="link_name" style="font-family: 'Kanit', sans-serif;">ข้อมูลพื้นฐาน</span>
          </a>
          <i class='bx bxs-chevron-down arrow' ></i>
        </div>
        <ul class="sub-menu">
          <li><a class="link_name" style="font-family: 'Kanit', sans-serif;">ข้อมูลพื้นฐาน</a></li>
          <li ><a href="Prename.php" style="font-family: 'Kanit', sans-serif;">- คำนำหน้าชื่อ</a></li>
          <li><a href="univercity.php" style="font-family: 'Kanit', sans-serif;">- มหาวิทยาลัย</a></li>
          <li><a href="faculty.php" style="font-family: 'Kanit', sans-serif;">- คณะ</a></li>
          <li><a href="department.php" style="font-family: 'Kanit', sans-serif;">- ภาควิชา</a></li>
          <li><a href="course.php" style="font-family: 'Kanit', sans-serif;">- หลักสูตร</a></li>
          <li><a href="subject.php" style="font-family: 'Kanit', sans-serif;">- รายวิชา</a></li>
        </ul>
      </li>
      <li>
    <div class="profile-details">
    <div class="profile-content">
        <!-- <img src="image/profile.jpg" alt="profileImg"> -->
        <img src="image/logo1.png" alt="profileImg" style="width: 55px;  height:55px;">
      </div>
      <?php while($row=mysqli_fetch_array($result)){ ?>
    <a href="editprofile.php">
      <div class="name-job">
        <div class="profile_name" style="font-family: 'Kanit', sans-serif; font-size: 14px;"><?php echo $row['teacher_fname'];?> <?php echo $row['teacher_lname'];?></div>
        <div class="job" style="font-family: 'Kanit', sans-serif;">Teacher</div>
      </div>
    </a>
      <?php }?>
      <a href="hometeacher1.php?logout='1'">
        <i class='bx bx-log-out' ></i>
      </a>
    </div>
  </li> 
</ul>
  </div>
  <section class="home-section">
    <div class="home-content">
      <i class='bx bx-menu' ></i>
      <span class="text">Online Education</span>
    </div>
    <div id="page-content-wrapper">
            <div class="container-fluid px-4">
                <!-- <div class="row g-3 my-2">
                    <div class="col-md-3">
                        <div class="p-3 bg-white shadow-sm d-flex justify-content-around align-items-center rounded">
                            <div>
                                <h3 class="fs-2"><?php //echo $op["totalop"];?></h3>
                                <p class="fs-5">รายวิชาที่เปิดสอน</p>
                            </div>
                            <i class="fas fa-book fs-1 primary-text border rounded-full secondary-bg p-3"></i>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="p-3 bg-white shadow-sm d-flex justify-content-around align-items-center rounded">
                            <div>
                                <h3 class="fs-2"><?php // echo $std["totalstd"];?></h3>
                                <p class="fs-5">นิสิตในทุกรายวิชา</p>
                            </div>
                            <i
                                class="fas fa-users fs-1 primary-text border rounded-full secondary-bg p-3"></i>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="p-3 bg-white shadow-sm d-flex justify-content-around align-items-center rounded">
                            <div>
                                <h3 class="fs-2"><?php //echo $sub["totalsub"];?></h3>
                                <p class="fs-5">รายวิชา</p>
                            </div>
                            <i class="fas fa-download fs-1 primary-text border rounded-full secondary-bg p-3"></i>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="p-3 bg-white shadow-sm d-flex justify-content-around align-items-center rounded">
                            <div>
                                <h3 class="fs-2"><?php //echo $doc["totaldoc"];?></h3>
                                <p class="fs-5">เอกสารประกอบการสอน</p>
                            </div>
                            <i class="fas fa-download fs-1 primary-text border rounded-full secondary-bg p-3"></i>
                        </div>
                    </div>
                </div> -->

                <div class="row my-5">
                    <h3 class="fs-4 mb-3">รายวิชาที่เปิดสอน</h3>
                    <div class="col">
                       
                      <?php $i=0; while($row=mysqli_fetch_array($nquery)){ $i=$i+1 ?>
                          <h4><?php echo $row["subject_engname"] ?></h4>
                          <div class="tab">
                            <button class="tablinks" onclick="openCity(event, 'std<?php echo $i ?>')">นิสิตในรายวิชา</button>
                            <button class="tablinks" onclick="openCity(event, 'doc<?php echo $i ?>')">เอกสารการสอน</button>
                            <button class="tablinks" onclick="openCity(event, 'vdo<?php echo $i ?>')">วีดิทัศน์</button>
                            <button class="tablinks" onclick="openCity(event, 'work<?php echo $i ?>')">แบบฝึกหัด</button>
                            <button class="tablinks" onclick="openCity(event, 'exam<?php echo $i ?>')">ข้อสอบ</button>
                            <button class="tablinks" onclick="openCity(event, 'check<?php echo $i ?>')">ตรวจข้อสอบ</button>
                          </div>

                          <div id="std<?php echo $i ?>" class="tabcontent">
                            <!-- <h5>นิสิตในรายวิชา</h5> -->
                            <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#staticBackdrop" >เพิ่มข้อมูลนิสิตในรายวิชา</button>
                            <br><br>
                             <table class="table bg-white rounded shadow-sm  table-hover">
                            <thead>
                                <tr>
                                    <th scope="col" width="50">ลำดับ</th>
                                    <th scope="col" style="width: 20px;">รหัสนิสิต</th>
                                    <th scope="col" style="width: 90px;">ชื่อ-นามสกุล</th>
                                </tr>
                            </thead>
                            <?php 
                              $rowstd=$row["coursesopen_id"];
                              $study="SELECT study.study_id,subject.subject_engname,student.student_id,student.student_fname,student.student_lname,study.study_status
                              FROM study 
                              INNER JOIN coursesopen ON study.study_coursesopen_id=coursesopen.coursesopen_id
                              INNER JOIN subject ON coursesopen.coursesopen_subject_id=subject.subject_id
                              INNER JOIN student ON study.study_student_id=student.student_id
                              WHERE study_coursesopen_id=".$rowstd;
                              $stdstudy=mysqli_query($conn,$study);
                            ?>
                            <tbody><?php $std=0; while($stu=mysqli_fetch_array($stdstudy)){ $std=$std+1 ?>
                                <tr>
                                    <th scope="row"><?php echo $std ?></th>
                                    <td><?php echo $stu["student_id"] ?></td>
                                    <td><?php echo $stu["student_fname"]." ".$stu["student_lname"] ?></td>
                                   
                                </tr>
                                <?php } ?>
                            </tbody> 
                            </table>
                          </div>

                          <div id="doc<?php echo $i ?>" class="tabcontent">
                            <h3>เอกสารการสอน</h3>
                            <table>
              <thead>
                <tr>
                  <th scope="col">ลำดับ</th>
                  <th scope="col">รายวิชา</th>
                  <th scope="col">ชื่อเอกสาร</th>
                  <th scope="col">ไฟล์เอกสารประกอบการสอน</th>
                 <th scope="col">สถานะการใช้งาน</th>
                  <th scope="col">แก้ไขข้อมูล</th>
                  <th scope="col">รายละเอียด</th>
                  
                </tr>
              </thead>
              <tbody id="document">
              <tr>
              <?php $i=0;
                foreach($result1 as $row){ 
                    if ($row["document_status"]==1) {
                      $status= '<div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" id="icon'.$row["document_id"].'" checked>
                          </div>';
                    }
                    else if ($row["document_status"]==0) {
                      $status='<div class="form-check form-switch">
                            <input class="form-check-input" type="checkbox" id="icon'.$row["document_id"].'">
                          </div>';
                    }
                   ?> <script>
                          $(function() {
                            $('#icon<?php echo $row["document_id"];?>').change(function() {
                            var ch_val = $(this).prop('checked');
                            // var rel = $(this).val();
                            var rel = <?php echo $row["document_id"];?>;

                            if(ch_val==true){
                              var status = 1;
                            }
                            if(ch_val==false){
                              var status = 0;
                            }

                            $.ajax({
                              url: 'status/statusdoc.php',
                              type: 'POST',
                              data: {id: rel, value: status},
                              success: function (data) {
                                console.log(data);
                                }
                              });

                          
                            })
                          })
                    </script>
                <?php
                  $i=$i+1;
                  echo '<tr>
                      <td> '.$i.'</td>
                      <td data-label="รายวิชา">'.$row["subject_engname"].'</td>
                      <td data-label="ชื่อวิชา">'.$row['document_name'].'</td>
                      <td data-label="ไฟล์เอกสารประกอบการสอน"><a href="uploadbook/'.$row["document_file"].'">ดาวน์โหลด</a></td>
                      <td data-label="สถานะการใช้งาน">'.$status.'</td>
                      <td><button type="button" name="edit"  id="'.$row["document_id"].'" class="btn btn-info btn-xs edit_data"><i class="ri-edit-2-fill"></i></button> </td>  
                      <td>
                        <button type="button" name="view" value="view" data-bs-target="#staticBackdrop" id="'.$row["document_id"].'" class="btn btn-info btn-xs view_data"><i class="ri-eye-fill"></i></button>
                      </td>  
                    </tr>'; 
                   } ?>
                </tr>
              </tbody>
            </table>
                          </div>

                          <div id="vdo<?php echo $i ?>" class="tabcontent">
                            <h3>วีดิทัศน์</h3>
                            <p>Tokyo is the capital of Japan.</p>
                          </div>

                          <div id="work<?php echo $i ?>" class="tabcontent">
                            <h3>แบบฝึกหัด</h3>
                            <p>Tokyo is the capital of Japan.</p>
                          </div>

                          <div id="exam<?php echo $i ?>" class="tabcontent">
                            <h3>ข้อสอบ</h3>
                            <p>Tokyo is the capital of Japan.</p>
                          </div>

                          <div id="check<?php echo $i ?>" class="tabcontent">
                            <h3>ตรวจข้อสอบ</h3>
                            <p>Tokyo is the capital of Japan.</p>
                          </div>
                          <br><br>
                          <?php }?>
                        <div id="pagination_controls" style="font-family: Kanit, sans-serif;"><?php echo $paginationCtrls; ?></div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!-- /#page-content-wrapper -->
    </div>
  </section>

  <script src="menu/script.js"></script>

</body>
</html>
<script>
function openCity(evt, cityName) {
  var i, tabcontent, tablinks;
  tabcontent = document.getElementsByClassName("tabcontent");
  for (i = 0; i < tabcontent.length; i++) {
    tabcontent[i].style.display = "none";
  }
  tablinks = document.getElementsByClassName("tablinks");
  for (i = 0; i < tablinks.length; i++) {
    tablinks[i].className = tablinks[i].className.replace(" active", "");
  }
  document.getElementById(cityName).style.display = "block";
  evt.currentTarget.className += " active";
}
</script>
<!-- Modal -->
<div class="modal fade" id="staticBackdrop" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true" >
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                  <h5 class="modal-title font-color" id="staticBackdropLabel" > เพิ่มข้อมูลนิสิตในรายวิชา</h5>
                  <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                  <form class="row g-3 needs-validation" novalidate action="../teacher/Add/instudy.php" method="POST">
                    <label for="validationCustom01" class="form-label" >รายวิชา</label>
                    <select class="form-select form-control" aria-label="Default select example" name="study_coursesopen_id">
                        <option selected disabled>-เลือกรายวิชา-</option>
                        <?php
                              while($rows=mysqli_fetch_row($result73)){
                                  $uni_id=$rows[0];
                                  $uni_name=$rows[1];
                                  echo "<option value='$uni_id'>$uni_name</option>";
                              }
                          ?> 
                      </select>
                      
                    <div>
                        <label for="validationCustom01" class="form-label" >รหัสนิสิต</label>
                        <input type="text" required class="form-control" placeholder="กรอกรหัสนิสิต"  name="study_student_id">
                      </div>
                  
                </div>
                <div class="modal-footer">
                  <!-- <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button> -->
                  <button type="submit" class="btn btn-success">บันทึกข้อมูล</button>
                </div>
                </form>
              </div>
            </div>
          </div>